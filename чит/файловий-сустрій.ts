import { createReadStream, existsSync, mkdirSync } from "fs";
import { dirname } from "path";

export function ручитиИснуванняПутіФайлаОБ(путь: string) {
  const путьПапки = dirname(путь);
  ручитиИснуванняПутіПапкиОБ(путьПапки);
}

export function ручитиИснуванняПутіПапкиОБ(путьПапки: string) {
  if (!existsSync(путьПапки)) {
    mkdirSync(путьПапки, { recursive: true });
  }
}

export function читатиПервіРядки(путь: string, ксть: number) {
  return new Promise(function (resolve, reject) {
    const rs = createReadStream(путь, { encoding: "utf8" });
    let потКсть = 0;
    let acc = "";
    let pos = 0;
    let index: number;
    rs.on("data", function (chunk) {
      index = chunk.indexOf("\n");
      acc += chunk;
      потКсть++;
      index !== -1 || потКсть === ксть ? rs.close() : (pos += chunk.length);
    })
      .on("close", function () {
        resolve(acc.slice(0, pos + index));
      })
      .on("error", function (err) {
        reject(err);
      });
  });
}
