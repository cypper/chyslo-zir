import { parse } from "csv-parse/sync";
import { createReadStream, readFileSync } from "fs";
import { Числина, Картина, ЧИСЛИНА } from "./картина-число";
import { PNG } from "pngjs";
import { resolve } from "path";

const input = `
"key_1","key_2"
"value 1","value 2"
`;

export type ДаніКартинка = [Картина, (typeof ЧИСЛИНА)[number]];
export const СПИТНІ_КАРТИНКИ: ДаніКартинка[] = (
  parse(readFileSync("./дані/mnist_test.csv"), {
    skip_empty_lines: true,
  }) as string[][]
)
  .filter(([числина]) =>
    (ЧИСЛИНА as readonly number[]).includes(parseInt(числина)),
  )
  .map(
    ([числина, ...дані]) =>
      [
        new Картина(дані.map((а) => parseInt(а))),
        parseInt(числина) as Числина,
      ] as const,
  );

// export const ЧОВПНІ_КАРТИНКИ = [];
export const ЧОВПНІ_КАРТИНКИ: ДаніКартинка[] = (
  parse(readFileSync("./дані/mnist_train.csv"), {
    skip_empty_lines: true,
  }) as string[][]
)
  .filter(([числина]) =>
    (ЧИСЛИНА as readonly number[]).includes(parseInt(числина)),
  )
  .map(
    ([числина, ...дані]) =>
      [
        new Картина(дані.map((а) => parseInt(а))),
        parseInt(числина) as Числина,
      ] as const,
  );

export function pngВЧорноБілийРяд(путь: string) {
  const ряд: number[] = [];
  PNG.sync.read(readFileSync(путь)).data.forEach((v, вк) => {
    if ((вк + 1) % 4 === 1) {
      ряд.push(v);
    }
  });
  return ряд;
}

export const РУЧНІ_КАРТИНКИ: ДаніКартинка[] = (
  [
    [3, "./дані/картинки/3.png"],
    [2, "./дані/картинки/?2.png"],
    [8, "./дані/картинки/8.png"],
    [6, "./дані/картинки/6.png"],
  ] as const
)
  .filter(([числина]) => (ЧИСЛИНА as readonly number[]).includes(числина))
  .map(([числина, путь]) => {
    const дані = pngВЧорноБілийРяд(путь);

    return [new Картина(дані), числина as ДаніКартинка[1]] as const;
  });
