export const ЧИСЛИНА = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] as const;
export type Числина = (typeof ЧИСЛИНА)[number];

export class Картина {
  static ВИСОТА = 28;
  static ШИРИНА = 28;

  дані: number[][];
  спрощеніДані: number[][];

  constructor(дані: number[]) {
    if (дані.length !== Картина.ВИСОТА * Картина.ШИРИНА) throw new Error("Упс");

    this.дані = дані
      .map((v) => v / 255)
      .reduce((rows, key, index) => {
        index % Картина.ШИРИНА == 0
          ? rows.push([key])
          : rows[rows.length - 1].push(key);
        return rows;
      }, [] as number[][]);

    this.спрощеніДані = this.дані
      .filter((e, i) => i % 2 === 0)
      .map((р, вк1) => р.filter((e, i) => i % 2 === 0));
  }

  відстаньДо(картина: Картина) {
    const похибки = this.спрощеніДані.map((р, вк1) =>
      р.map((п, вк2) => (п - картина.спрощеніДані[вк1][вк2]) ** 4),
    );
    return похибки.reduce(
      (сума, п) => сума + п.reduce((сума, п) => сума + п, 0),
      0,
    );
  }
}
