import tf from "./tf";
import { ДаніКРСлужба } from "./дані-служба";
import { Картина, ЧИСЛИНА, Числина } from "../картина/картина-число";
import { СПИТНІ_КАРТИНКИ, ЧОВПНІ_КАРТИНКИ } from "../картина/готові-картинки";
import { Matrix } from "ml-matrix";
import { Tensor } from "@tensorflow/tfjs-node";
import { зтяженіВипадкові, однобіжно } from "../обробні";
import { ручитиИснуванняПутіПапкиОБ } from "../файловий-сустрій";
import { readFileSync, writeFileSync } from "fs";

type ВхДані = Картина;
type ВихДані = Числина;

type ТварВхДаних = tf.Rank.R2;
type ТварВихДаних = tf.Rank.R1;

type ВхДаніКР = tf.Tensor<ТварВхДаних>;
type ВихДаніКР = tf.Tensor<ТварВихДаних>;

interface БгалПарсептрона {
  розмірШаруС: number;
  розмірШаруА: number;
  розмірШаруР: number;
  порігШаруС: number;
  порогиШаруА: Matrix;
  порогиШаруР: Matrix;
  тяжіМіжшарряСА: Matrix;
  тяжіМіжшарряАР: Matrix;
}

// САР
export class КартинаВЧислинуПарсептрон {
  // readonly розмірВхіднихДаних: number;
  // readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  // readonly розмірВихіднихДаних: number;
  // readonly твархіднихДаних: number;
  // readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  // readonly розмірВихіднихДаних: number;
  // readonly тварВихВихіднихДаних: tf.ShapeMap[ТварВихДаних];

  constructor(private readonly даніКРСлужба: ДаніКРСлужба) {}

  створитиБгал(): БгалПарсептрона {
    const розмірШаруС = this.даніКРСлужба.картинаДані().розмір;
    const розмірШаруА = Math.round(розмірШаруС * 5);
    const розмірШаруР = this.даніКРСлужба.числинаДані().розмір;

    const тяжіМіжшарряСА = new Matrix(
      Array(розмірШаруС)
        .fill(0)
        .map(() => {
          return Array(розмірШаруА)
            .fill(0)
            .map(() => {
              return Math.floor(Math.random() * 3) - 1;
            });
        }),
    );

    const тяжіМіжшарряАР = new Matrix(
      Array(розмірШаруА)
        .fill(0)
        .map(() => {
          return Array(розмірШаруР).fill(0);
        }),
    );

    const порогиШаруА = new Matrix([
      Array(розмірШаруА)
        .fill(0)
        .map(() => {
          return Math.random() * 10 - 5;
        }),
    ]);
    const порогиШаруР = new Matrix([
      Array(розмірШаруР)
        .fill(0)
        .map(() => {
          return Math.random() * 10 - 5;
        }),
    ]);

    return {
      розмірШаруС,
      розмірШаруА,
      розмірШаруР,
      порігШаруС: 150,
      порогиШаруА,
      порогиШаруР,
      тяжіМіжшарряСА,
      тяжіМіжшарряАР,
    };
  }

  private поправитиТяжі(бгал: БгалПарсептрона, шарА: Matrix, помилки: Tensor) {
    for (const [вк, помилка] of (помилки.arraySync() as number[]).entries()) {
      if (помилка === 0) continue;

      const тяжіПервняР = бгал.тяжіМіжшарряАР.getColumnVector(вк).transpose();
      const новіТяжіПервняР =
        помилка > 0 ? тяжіПервняР.add(шарА) : тяжіПервняР.sub(шарА);

      бгал.тяжіМіжшарряАР.setColumn(вк, новіТяжіПервняР.transpose());
    }
  }

  async човпити(бгал: БгалПарсептрона) {
    for (let пВк = 0; пВк < 20; пВк++) {
      const рядДаних = await this.картинкиВРядДанихКР(
        await this.витягЧовпніКартинки(),
      );

      console.log("Повтор ", пВк + 1);
      const вгадів = Array(ЧИСЛИНА.length).fill(0);
      const невгадів = Array(ЧИСЛИНА.length).fill(0);
      for (let рВк = 0; рВк < рядДаних.многоВхДаніКР.length; рВк++) {
        const вх = рядДаних.многоВхДаніКР[рВк];
        const истВих = рядДаних.многоВихДаніКР[рВк];

        const [шарА, шарР, потВих] = this.віщуватиКР(бгал, вх);

        const истШарР = истВих.arraySync().map((ч) => ч > 0);
        const потШарР = потВих.arraySync().map((ч) => ч > 0);
        const помилки = истВих.sub(потВих);

        this.поправитиТяжі(бгал, шарА, помилки);

        if (истШарР.every((р, вк) => потШарР[вк] === р)) {
          вгадів[истШарР.findIndex((р) => р)]++;
        } else {
          невгадів[истШарР.findIndex((р) => р)]++;
        }
      }
      const заг = вгадів.reduce((acc, p) => acc + p, 0);
      const загНевг = невгадів.reduce((acc, p) => acc + p, 0);
      console.log(
        `Вислід ${(100 * заг) / (заг + загНевг)}% [${заг + загНевг}]`,
      );
      вгадів.map((вг, вк) => {
        console.log(
          `Вислід ${вк} ${(100 * вг) / (вг + невгадів[вк])}% [${вг + невгадів[вк]}]`,
        );
      });
      console.log(бгал.тяжіМіжшарряАР);
    }
  }

  async перевірити(
    бгал: БгалПарсептрона,
    рядДаних: { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] },
  ) {
    const вгадів = Array(ЧИСЛИНА.length).fill(0);
    const невгадів = Array(ЧИСЛИНА.length).fill(0);
    for (let рВк = 0; рВк < рядДаних.многоВхДаніКР.length; рВк++) {
      const вх = рядДаних.многоВхДаніКР[рВк];
      const истВих = рядДаних.многоВихДаніКР[рВк];

      const [, , потВих] = this.віщуватиКР(бгал, вх);

      const истШарР = истВих.arraySync().map((ч) => ч > 0);
      const потШарР = потВих.arraySync().map((ч) => ч > 0);

      if (истШарР.every((р, вк) => потШарР[вк] === р)) {
        вгадів[истШарР.findIndex((р) => р)]++;
      } else {
        невгадів[истШарР.findIndex((р) => р)]++;
        // console.log('Числина пот:', потШарР.map((р,вк) => [вк, р]),', ист:', истШарР.findIndex((р) => р))
      }
    }
    const заг = вгадів.reduce((acc, p) => acc + p, 0);
    const загНевг = невгадів.reduce((acc, p) => acc + p, 0);
    console.log(`Вислід ${(100 * заг) / (заг + загНевг)}% [${заг + загНевг}]`);
    вгадів.map((вг, вк) => {
      console.log(
        `Вислід ${вк} ${(100 * вг) / (вг + невгадів[вк])}% [${вг + невгадів[вк]}]`,
      );
    });
  }

  віщуватиКР(
    бгал: БгалПарсептрона,
    вхДані: ВхДаніКР,
  ): [Matrix, Matrix, ВихДаніКР] {
    const шарС = new Matrix([
      вхДані.as1D().arraySync(),
      // .map((зн) => this.пороговаФункція(зн, бгал.порігШаруС)),
    ]);
    const шарА = new Matrix([
      шарС
        .mmul(бгал.тяжіМіжшарряСА)
        .to1DArray()
        .map((зн, вк) => this.пороговаФункція(зн, бгал.порогиШаруА.get(0, вк))),
    ]);
    const шарР = new Matrix([
      шарА
        .mmul(бгал.тяжіМіжшарряАР)
        .to1DArray()
        .map((зн, вк) => this.пороговаФункція(зн, бгал.порогиШаруР.get(0, вк))),
    ]);

    return [шарА, шарР, tf.tensor1d(шарР.to1DArray())];
  }

  пороговаФункція(значення: number, поріг: number) {
    return значення >= поріг ? 1 : 0;
  }

  async витягЧовпніКартинки() {
    return зтяженіВипадкові(ЧОВПНІ_КАРТИНКИ, () => 1, 5000);
  }

  async картинкиВРядДанихКР(дані: typeof СПИТНІ_КАРТИНКИ) {
    const човпніДані = await однобіжно(дані, async (д) => {
      return [await this.вВхДаніКР(д[0]), await this.вВихДаніКР(д[1])] as const;
    });

    return {
      многоВхДаніКР: човпніДані.map((д) => д[0]),
      многоВихДаніКР: човпніДані.map((д) => д[1]),
    };
  }

  async вВхДаніКР(дані: ВхДані) {
    return tf.tensor2d(this.даніКРСлужба.картинаВДаніКР(дані));
  }

  async вВихДаніКР(дані: ВихДані) {
    return tf.tensor1d(this.даніКРСлужба.числинаВДаніКР(дані));
  }

  async ввізБгалу(путьПапки: string) {
    const json = JSON.parse(readFileSync(`${путьПапки}/бгал.json`, "utf-8"));
    return {
      розмірШаруС: json.розмірШаруС,
      розмірШаруА: json.розмірШаруА,
      розмірШаруР: json.розмірШаруР,
      порігШаруС: json.порігШаруС,
      порогиШаруА: new Matrix(json.порогиШаруА),
      порогиШаруР: new Matrix(json.порогиШаруР),
      тяжіМіжшарряСА: new Matrix(json.тяжіМіжшарряСА),
      тяжіМіжшарряАР: new Matrix(json.тяжіМіжшарряАР),
    };
  }

  async вивізБгалу(путьПапки: string, бгал: БгалПарсептрона): Promise<void> {
    ручитиИснуванняПутіПапкиОБ(путьПапки);
    writeFileSync(
      `${путьПапки}/бгал.json`,
      JSON.stringify({
        розмірШаруС: бгал.розмірШаруС,
        розмірШаруА: бгал.розмірШаруА,
        розмірШаруР: бгал.розмірШаруР,
        порігШаруС: бгал.порігШаруС,
        порогиШаруА: бгал.порогиШаруА.to2DArray(),
        порогиШаруР: бгал.порогиШаруР.to2DArray(),
        тяжіМіжшарряСА: бгал.тяжіМіжшарряСА.to2DArray(),
        тяжіМіжшарряАР: бгал.тяжіМіжшарряАР.to2DArray(),
      }),
    );
  }
}
