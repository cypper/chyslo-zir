import { writeFileSync } from "node:fs";
import tf from "./tf";
import { БгалКР, УявнийКутийРозум } from "./уявний-кр";
import { ДаніКРСлужба } from "./дані-служба";
import { зтяженіВипадкові, однобіжно } from "../обробні";
import { ручитиИснуванняПутіФайлаОБ } from "../файловий-сустрій";
import { Картина, ЧИСЛИНА, Числина } from "../картина/картина-число";
import { СПИТНІ_КАРТИНКИ } from "../картина/готові-картинки";

type ВхДані = Картина;
type ВихДані = Числина;

type ТварВхДаних = tf.Rank.R2;
type ТварВихДаних = tf.Rank.R1;

type ВхДаніКР = tf.Tensor<ТварВхДаних>;
type ВихДаніКР = tf.Tensor<ТварВихДаних>;

export class КартинаВЧислинуКутийРозум extends УявнийКутийРозум<
  ВхДані,
  ВихДані,
  ВхДаніКР,
  ВихДаніКР
> {
  readonly розмірВхіднихДаних: number;
  readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  readonly розмірВихіднихДаних: number;
  readonly тварВихіднихДаних: tf.ShapeMap[ТварВихДаних];
  readonly пострійЧовпення: Partial<
    tf.ModelFitArgs & tf.ModelFitDatasetArgs<{ xs: tf.Tensor; ys: tf.Tensor }>
  >;

  constructor(
    private readonly даніКРСлужба: ДаніКРСлужба,
    verbose: 0 | 1 = 1,
  ) {
    super();

    this.пострійЧовпення = {
      verbose,
    };

    this.розмірВхіднихДаних = даніКРСлужба.картинаДані().розмір;
    this.тварВхіднихДаних = даніКРСлужба.картинаДані().твар;
    this.розмірВихіднихДаних = даніКРСлужба.числинаДані().розмір;
    this.тварВихіднихДаних = даніКРСлужба.числинаДані().твар;
  }

  створитиБгал() {
    const бгал = tf.sequential();

    const серРяд = Math.round(this.розмірВхіднихДаних);

    бгал.add(
      tf.layers.inputLayer({
        inputShape: this.тварВхіднихДаних,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: [this.розмірВхіднихДаних],
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: серРяд,
        activation: "relu",
        // biasInitializer: 'zeros',
        // kernelInitializer: tf.initializers.,
        weights: [
          tf.tensor(
            Array(this.розмірВхіднихДаних)
              .fill(0)
              .map(() => {
                return Array(серРяд)
                  .fill(0)
                  .map(() => {
                    return Math.floor(Math.random() * 3) - 1;
                  });
              }),
            [this.розмірВхіднихДаних, серРяд],
          ),

          tf.tensor(
            Array(серРяд)
              .fill(0)
              .map(() => {
                return 0;
              }),
            [серРяд],
          ),
        ],
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: this.розмірВихіднихДаних,
        activation: `relu`,
        biasInitializer: "zeros",
        kernelInitializer: "zeros",
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: this.тварВихіднихДаних,
      }),
    );

    this.укласти(бгал);

    return бгал;
  }

  private укласти(бгал: БгалКР) {
    // бгал.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
    бгал.compile({
      // loss: (исть: tf.Tensor, пит: tf.Tensor) => {
      //   // исть.print()
      //   // console.log(исть.shape)
      //   // пит.print()
      //   // console.log(пит.shape)
      //   // исть.sub(исть.mul(пит)).print()

      //   return tf.sum(исть.sub(исть.mul(пит)), -1);
      // },
      loss: "meanSquaredError",
      // loss: tf.losses.sigmoidCrossEntropy,
      optimizer: "sgd",
      // optimizer: tf.train.adamax(0.01),
      // optimizer: tf.train.momentum(0.1, 0.9),
      // metrics: [
      //   "accuracy",
      //   tf.metrics.binaryAccuracy,
      //   tf.metrics.binaryCrossentropy,
      // ],
    });
  }

  async створитиЧовпніДаніКР(): Promise<void> {
    ручитиИснуванняПутіФайлаОБ(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
    );
    writeFileSync(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
      JSON.stringify(await this.витягЧовпніКартинки()),
    );
  }

  private async витягЧовпніКартинки() {
    return зтяженіВипадкові(СПИТНІ_КАРТИНКИ, () => 1, 1000);
  }

  async витягЧовпнийНабірДанихКР() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;

    const data = await that.витягЧовпніКартинки();

    const створювач = async function* () {
      for (const дані of data) {
        yield {
          xs: await that.вВхДаніКР(дані[0]),
          ys: await that.вВихДаніКР(дані[1]),
        };
        // console.log('---', дані[0][1]. поле, дані[1])
        // a.xs.print()
        // a.ys.print()
        // console.log('---e')

        // yield a
      }
    } as unknown as () => Generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>;

    return tf.data
      .generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>(створювач)
      .batch(32) as tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>;
  }

  async витягЧовпнийРядДанихКР() {
    const дані = await this.витягЧовпніКартинки();
    const човпніДані = await однобіжно(дані, async (д) => {
      return [await this.вВхДаніКР(д[0]), await this.вВихДаніКР(д[1])] as const;
    });

    return {
      многоВхДаніКР: човпніДані.map((д) => д[0]),
      многоВихДаніКР: човпніДані.map((д) => д[1]),
    };
  }

  private примножитиТяжі(
    бгал: БгалКР,
    рядИстЗбуджень: boolean[],
    рядЗбуджень: boolean[],
    примножник: number,
    додавач: number,
    примножник2: number,
    додавач2: number,
  ) {
    // TODO: утік памяті при множені чи додаванні тяжів

    // console.log(рядЗбігів)
    const новіТяжіТенсори = бгал.getWeights();
    const новіТяжі2Прошару = (новіТяжіТенсори[2].arraySync() as number[][]).map(
      (р) => {
        return р.map((зн, вк) => {
          if (!рядЗбуджень[вк] && рядИстЗбуджень[вк]) {
            return зн + додавач;
          } else if (рядЗбуджень[вк] && !рядИстЗбуджень[вк]) {
            return зн + додавач2;
          }

          if (рядЗбуджень[вк] === рядИстЗбуджень[вк]) {
            return зн * примножник;
          } else {
            return зн * примножник2;
          }

          return зн;
        });
      },
    );
    // новіТяжіТенсори[2].print()
    новіТяжіТенсори[2] = новіТяжіТенсори[2].mul(0).add(новіТяжі2Прошару);
    // новіТяжіТенсори[2].print()

    // console.log(новіТяжіТенсори)
    бгал.setWeights(новіТяжіТенсори);
    // tf.dispose(новіТяжіТенсори)
  }

  async човпитиЯкПарсептрон(
    бгал: БгалКР,
    рядДаних: { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] },
  ) {
    for (let пВк = 0; пВк < 100; пВк++) {
      console.log("Повтор ", пВк + 1);
      const вгадів = Array(ЧИСЛИНА.length).fill(0);
      const невгадів = Array(ЧИСЛИНА.length).fill(0);
      for (let рВк = 0; рВк < рядДаних.многоВхДаніКР.length; рВк++) {
        const вх = рядДаних.многоВхДаніКР[рВк];
        const истВих = рядДаних.многоВихДаніКР[рВк];

        const потВих = this.віщуватиКР(бгал, вх);

        const истЧислина = истВих.arraySync().map((ч) => ч > 0)[0] ? 0 : 1;
        const потЧислина = потВих.arraySync().map((ч) => ч > 0)[0] ? 0 : 1;

        //  console.log( истЧислина)
        this.примножитиТяжі(
          бгал,
          [истЧислина === 0],
          [потЧислина === 0],
          1.01,
          1,
          0.99,
          -1,
        );

        if (истЧислина === потЧислина) {
          вгадів[истЧислина]++;
        } else {
          невгадів[истЧислина]++;
        }
      }
      const заг = вгадів.reduce((acc, p) => acc + p, 0);
      const загНевг = невгадів.reduce((acc, p) => acc + p, 0);
      console.log(
        `Вислід ${(100 * заг) / (заг + загНевг)}% [${заг + загНевг}]`,
      );
      вгадів.map((вг, вк) => {
        console.log(
          `Вислід ${вк} ${(100 * вг) / (вг + невгадів[вк])}% [${вг + невгадів[вк]}]`,
        );
      });
      console.log(tf.memory().numTensors, бгал.getWeights().length);
      бгал.getWeights()[2].print();
    }
  }

  async човпити(
    бгал: БгалКР,
    рядАбоНабірДаних:
      | { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] }
      | tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>,
    волі?: { крятПоступу?: tf.CustomCallbackArgs },
  ) {
    const прогонів = 100;

    if (рядАбоНабірДаних instanceof tf.data.Dataset) {
      await бгал.fitDataset(рядАбоНабірДаних, {
        epochs: прогонів,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    } else {
      const злученіВхДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВхДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВхДаніКР.length, ...this.тварВхіднихДаних],
      );
      const злученіВихДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВихДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВихДаніКР.length, ...this.тварВихіднихДаних],
      );

      await бгал.fit(злученіВхДаніКР, злученіВихДаніКР, {
        epochs: прогонів,
        shuffle: true,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    }
  }

  віщуватиКР(бгал: БгалКР, вхДані: ВхДаніКР): ВихДаніКР {
    const віщаКР = бгал.predict(вхДані.as3D(1, ...вхДані.shape)) as ВихДаніКР;
    if (Array.isArray(віщаКР)) throw new Error("Неочікуваний масив");
    return віщаКР.as1D();
  }

  async віщувати(бгал: БгалКР, вхДані: ВхДані): Promise<ВихДані> {
    const вихДаніКР = this.віщуватиКР(бгал, await this.вВхДаніКР(вхДані));
    // вихДаніКР.print();
    return this.зВихДанихКР(вихДаніКР);
  }

  async вВхДаніКР(дані: ВхДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    return tf.tensor2d(this.даніКРСлужба.картинаВДаніКР(дані));
  }

  async вВихДаніКР(дані: ВихДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    // return tf.tensor1d(this.даніКРСлужба.частинаМовиВДаніКР(дані));
    return tf.tensor1d(this.даніКРСлужба.числинаВДаніКР(дані));
    // return tf.tensor1d(дані ? [1] : [0]);
  }

  async зВихДанихКР(вихДаніКР: ВихДаніКР) {
    return this.даніКРСлужба.числинаЗДанихКР(вихДаніКР.arraySync());
  }

  async ввізБгалу(путьПапки: string) {
    const бгал = await tf.loadLayersModel(`file://${путьПапки}/model.json`);

    this.укласти(бгал);

    return бгал;
  }
}
