import { spawn, spawnSync } from "child_process";
import tf from "./tf";
import { БгалКР } from "./уявний-кр";

export class УвидженняСлужба {
  tfboardЧинописи: string;

  constructor() {
    this.tfboardЧинописи = `./тимч/човпні-чинописи`;
  }

  async відобразитиМережуВішня(кр: БгалКР) {
    кр.summary();
    for (const шар of кр.layers) {
      this.вивідВішня("-------шар поч-------");
      const [тяжі, упередження, ...рештаТяжів] = шар.getWeights();

      if (тяжі) {
        this.вивідВішня("Тяжі:");
        тяжі.print();
      }

      if (упередження) {
        this.вивідВішня("Упередження:");
        упередження.print();
      }

      if (рештаТяжів.length > 0) {
        this.вивідВішня("Решта тяжів:");
        рештаТяжів.map((тяжі) => тяжі.print());
      }

      this.вивідВішня("-------шар кін-------");
    }
  }

  async запуститиTfboard() {
    spawnSync("touch", ["-p", `${this.tfboardЧинописи}`], {
      cwd: process.cwd(),
    });
    const tb = spawn("tensorboard", ["--logdir", `${this.tfboardЧинописи}`], {
      cwd: process.cwd(),
    });

    tb.stdout.on("data", (data) => {
      this.вивідВішня(`tb: ${data}`);
    });

    tb.stderr.on("data", (data) => {
      this.вивідВішня(`tb error: ${data}`);
    });

    tb.on("close", (code) => {
      this.вивідВішня(`tb exited with code ${code}`);
    });
  }

  async крядДляПоступЧовпенняTfboard(
    i?: number,
  ): Promise<tf.CustomCallbackArgs> {
    return tf.node.tensorBoard(this.tfboardЧинописи + `/${i ?? "default"}`, {
      updateFreq: "batch",
      histogramFreq: 1,
    });
  }

  // async крядДляПоступЧовпенняFlowspace(кр: tf.Sequential): Promise<tf.CustomCallbackArgs> {
  //   // @ts-ignore
  //   const { HandShake } = await import("flowspace.js");
  //   const увиднювач =  new HandShake(кр);

  //   return {
  //     onEpochEnd: увиднювач.lossCallback
  //   }
  // }

  вивідВішня(пись: string | Array<string>) {
    // eslint-disable-next-line
    console.log(пись);
  }
}
